# redbag

#### 介绍
红包弹窗，纯前端


#### 安装教程

 直接引入模块

#### 使用说明

1. elm，mt 字段可根据需求更改

```javascript
info:{
	title:"测试标题",
	money:39.5,
	startTime:1,
	list:[
		{
			name:"测试红包",
			min:40,
			money:3,
			decimal:5
		}
	]
}
```

2. money,redbag [time] 是表示秒数

#### 

喜欢的话收藏，评论，赞赏吧
